package ressources;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.preference.PreferenceManager;
import android.util.Log;

import com.estragon.chessdiags2.Appli;
import com.estragon.chessdiags2.R;

public class Ressources {

	private static final int[] idPieces = new int[] {R.drawable.p0,R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4,R.drawable.p5,R.drawable.p6
		,R.drawable.p7,R.drawable.p8,R.drawable.p9,R.drawable.p10,R.drawable.p11,R.drawable.p12};
	public static final Bitmap[] pieces = new Bitmap[13];
	private static Bitmap echiquier = null;
	private static Bitmap echiquier2 = null;
	
	public synchronized static void charger() {
		new Thread(new Runnable() {
			public void run() {
				long millis = System.currentTimeMillis();
				for (int i = 0; i < pieces.length; i++) {
					if (pieces[i] != null) {
						continue;
					}
					pieces[i] = BitmapFactory.decodeResource(Appli.getInstance().getResources(), idPieces[i]);
				}
				if (echiquier == null) {
					echiquier = BitmapFactory.decodeResource(Appli.getInstance().getResources(), R.drawable.echiquier);
				}
				if (echiquier2 == null) {
					echiquier2 = BitmapFactory.decodeResource(Appli.getInstance().getResources(), R.drawable.echiquier2);
				}
				Log.i("Chessdiags","Ressources loaded in "+(System.currentTimeMillis()-millis)+" ms");
			}
		}).start();
	}
	
	public synchronized static void waitForLoad() {
		
	}
	
	public static Bitmap getEchiquier() {
		String echiquierPrefs = PreferenceManager.getDefaultSharedPreferences(Appli.getInstance()).getString("board", ""+R.drawable.echiquier);
		if (echiquierPrefs.equals("0")) 
			return echiquier;
		else return echiquier2;
	}
}
