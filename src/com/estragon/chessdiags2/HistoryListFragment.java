package com.estragon.chessdiags2;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.estragon.chessdiags2.ProblemListFragment.ListItemSelectedListener;

import core.History;
import core.Problem;

public class HistoryListFragment extends ListFragment implements OnItemLongClickListener {

	HistoryAdapter adapter;
	private ListItemSelectedListener selectedListener;
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		getListView().setFastScrollEnabled(true);
		setListAdapter(adapter = new HistoryAdapter(getActivity()));
		
		try {
			selectedListener = (ListItemSelectedListener) getActivity();
		} catch (ClassCastException e) {
			//e.printStackTrace();
		}
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Log.i("Click : ",""+position);
		notifySelected(((History)adapter.getItem(position)).getProblem());
	}
	
	private void notifySelected(Problem probleme) {
		if (selectedListener != null) {
			selectedListener.onListItemSelected(probleme);
		}
	}
	
	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		/*if (selectedListener != null) {
			selectedListener.onLongListItemSelected((Problem)adapter.getItem(arg2));
		}*/
		return true;
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		adapter.notifyDataSetChanged();
		adapter.notifyDataSetInvalidated();
		super.onResume();
	}
	

	
}
